class AppMailer < ActionMailer::Base
  default from: "support@ssupchang.com"

  def welcome_email(user)
    @user = user
    r = mail(to: @user.email, subject: "Welcome!")

  end
end