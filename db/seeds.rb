# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

30.times do
	series = Series.create(
		title: Faker::Lorem.sentence(4),
		description: Faker::Lorem.paragraph(2),
		no_of_lessons: Faker::Number.decimal(2),
		difficulty: "Intermediate",
		trailer: Faker::Lorem.words(4),
		rent_price: Faker::Number.number(2),
		buyout_price: Faker::Number.number(2)

	)
	p "----s---#{series.id}"
	10.times do
		episodes = series.episodes.create(
			episode_title: Faker::Lorem.sentence(4),
			episode_description: Faker::Lorem.paragraph(2),
			episode_video: "//content.jwplatform.com/players/CvHUmDHk-AoH2bczb.html",
		    episode_duration: rand(100)

		)


		p "---e----#{episodes.id}"
	end

end

