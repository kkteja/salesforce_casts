class CreateEpisodes < ActiveRecord::Migration
  def change
    create_table :episodes do |t|
      t.string :episode_title
      t.text :episode_description
      t.text :episode_video
      t.integer :series_id
      t.string :tags
      t.string :episode_type
      t.float :episode_duration

      t.timestamps null: false
    end
    add_index :episodes, :episode_title

    execute "SELECT setval('episodes_id_seq', 1000000)"    
  end

  
end
